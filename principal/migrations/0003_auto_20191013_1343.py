# Generated by Django 2.2.6 on 2019-10-13 13:43

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('principal', '0002_auto_20191013_1211'),
    ]

    operations = [
        migrations.RenameField(
            model_name='informacionpersonal',
            old_name='usuid',
            new_name='userid',
        ),
        migrations.RenameField(
            model_name='pregunta',
            old_name='usuid',
            new_name='userid',
        ),
    ]
