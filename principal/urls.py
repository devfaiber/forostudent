
from django.urls import path
from principal import views

app_name = "principal"
urlpatterns = [
    path('', views.inicio, name="index"),
    path('introducion/', views.introducion, name="introduccion"),
    path('justificacion/', views.justificacion, name="justificacion"),
    path('contacto/', views.contacto, name="contacto"),
    path('registro/', views.Registro.as_view(), name="registro"),
]
