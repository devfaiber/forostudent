from django.contrib import auth, messages
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse_lazy, reverse
from django.views.generic import CreateView
from django.contrib.auth.models import User
from django.views.generic.base import View
from principal.models import InformacionPersonal
from principal import forms




# Create your views here.
class Registro(CreateView):
    model = User
    template_name = "principal/registro.html"
    form_class = forms.RegistroDatosForm
    form2_class = forms.RegistroUserForm
    success_url = reverse_lazy("principal:index")

    def get_context_data(self, **kwargs):
        context = super(Registro, self).get_context_data(**kwargs)

        context['form'] = self.form_class(None, self.request.GET)
        if 'form2' not in context:
            context['form2'] = self.form2_class(self.request.GET)

        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object
        form = self.form_class(None,self.request.POST, self.request.FILES)
        form2 = self.form2_class(self.request.POST)

        if form.is_valid() and form2.is_valid():
            datosPersonales = form.save(commit=False) # no guarda lo tiene ya en lista
            datosPersonales.userid = form2.save()
            datosPersonales.save()

            messages.add_message(request, messages.SUCCESS, "El usuario se ha creado exitosamente ;)")

            return HttpResponseRedirect(self.get_success_url())
        else:
            messages.add_message(request,messages.ERROR,"recuerda rellenar todos los campos porfavor")
            messages.add_message(request, messages.WARNING, "Nota: si has editado el usuario podria ya estar en uso")

            return self.render_to_response(self.get_context_data(form=form, form2=form2))

class Login(View):
    def get(self,request):
        return HttpResponseRedirect(reverse("principal:index"))
    def post(self, request):
        username = request.POST.get("username", "").lower()
        password = request.POST.get("password", "")

        if username == "" and password == "":
            messages.add_message(request, messages.WARNING, "los campos no pueden estar vacios porfavor rellenarlos si es tan amable")
            return HttpResponseRedirect(reverse("principal:index"))

        user = auth.authenticate(username=username, password=password)
        if not user is None:
            auth.login(request, user)


            try:
                datos_personales = InformacionPersonal.objects.get(userid=user.pk)
            except InformacionPersonal.DoesNotExist:
                messages.add_message(request, messages.ERROR, "lo sentimos no hemos ubicado tus datos, comunicate con el administrador")
                return HttpResponseRedirect(reverse("logout"))

            if datos_personales.rol == "INS":
                return HttpResponseRedirect(reverse("instructor:index"))
            else:
                return HttpResponseRedirect(reverse("aprendiz:index"))

        else:
            messages.add_message(request, messages.ERROR, "usuario o contraseña incorrectos")

        return HttpResponseRedirect(reverse("principal:index"))

def inicio(request):

    try:
        datos_personales = InformacionPersonal.objects.get(userid=request.user.pk)
        rol = datos_personales.rol
    except:
        rol = None

    return render(request, 'principal/index.html',{"rol": rol})
def introducion(request):
    return render(request, 'principal/introduccion.html')
def justificacion(request):
    return render(request, 'principal/justificacion.html')
def contacto(request):
    return render(request, 'principal/contacto.html')

class Logout(View):
    def get(self, request):
        auth.logout(request)
        return HttpResponseRedirect(reverse('login'))
