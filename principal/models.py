from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
# Create your models here.

DOC_CHOICES = [
    ('CC','Cedula ciudadania'),
    ('CE','Cedula extranjera'),
    ('O',"Otro"),
]
ROL_CHOICES = [
    ("APR","Aprendiz"),
    ("INS","Instructor"),
]


class InformacionPersonal(models.Model):
    id = models.AutoField(primary_key=True)
    userid = models.OneToOneField(User, on_delete=models.CASCADE) #foranea a modelo User
    nombre = models.CharField(max_length=50)
    apellidos = models.CharField(max_length=50)
    tipo_documento = models.CharField(max_length=20, choices=DOC_CHOICES, default="CC")
    num_documento = models.CharField(max_length=20)
    rol = models.CharField(max_length=30, choices=ROL_CHOICES, default="APR")
    foto = models.ImageField(upload_to="perfiles/", null=True)
    estado = models.BooleanField(default=True)

    def __str__(self):
        return "%s %s"%(self.nombre,self.apellidos)

    class Meta:
        verbose_name = "Informacion Personal"
        verbose_name_plural = "Informacion Personales"


class Pregunta(models.Model):
    id = models.AutoField(primary_key=True)
    userid = models.ForeignKey(InformacionPersonal, on_delete=models.CASCADE, null=True)
    titulo = models.CharField(max_length=50)
    descripcion = models.TextField()
    fecha = models.DateField(default=timezone.now)
    estado = models.BooleanField(default=True)

    def __str__(self):
        return "%s"%(self.titulo)

    class Meta:
        verbose_name = "Pregunta"
        verbose_name_plural = "Preguntas"
        ordering = ['-id']


class Comentario(models.Model):
    id = models.AutoField(primary_key=True)
    userid = models.ForeignKey(InformacionPersonal, on_delete=models.CASCADE)
    pregid = models.ForeignKey(Pregunta, on_delete=models.CASCADE)
    contenido = models.TextField()
    fecha = models.DateField(default=timezone.now)
    estado = models.BooleanField(default=True)

    def __str__(self):
        return "%s | %s"% (self.userid, self.pregid)

    class Meta:
        verbose_name = "Comentario"
        verbose_name_plural = "Comentarios"
        ordering = ['-id']


class Respuesta(models.Model):
    id = models.AutoField(primary_key=True)
    userid = models.ForeignKey(InformacionPersonal, on_delete=models.CASCADE)
    pregid = models.ForeignKey(Pregunta, on_delete=models.CASCADE)
    comid = models.ForeignKey(Comentario, on_delete=models.CASCADE) # foranea a comentario
    contenido = models.TextField()

    estado = models.BooleanField(default=True)

    def __str__(self):
        return "%s | %s > %s"%(self.userid, self.pregid, self.comid)

    class Meta:
        verbose_name = "Respuesta de Comentario"
        verbose_name_plural = "Respuestas de Comentarios"