from django.urls import path
from instructor import views

app_name = "instructor"
urlpatterns = [
    path('', views.Index.as_view(), name="index"),
    path('editar_perfil/<int:pk>/', views.EditarPerfil.as_view(), name="editarPerfil"),
    path('preguntas/', views.VerPreguntas.as_view(), name="verPreguntas"),
    path('comentario/', views.InsertarComentario.as_view(), name="comentario"),
    path('lista-aprendices/', views.VerAprendices.as_view(), name="aprendices")
]

