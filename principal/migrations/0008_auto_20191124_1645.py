# Generated by Django 2.2.6 on 2019-11-24 16:45

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('principal', '0007_auto_20191124_1148'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='comentario',
            options={'ordering': ['-id'], 'verbose_name': 'Comentario', 'verbose_name_plural': 'Comentarios'},
        ),
    ]
