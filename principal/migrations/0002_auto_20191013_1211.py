# Generated by Django 2.2.6 on 2019-10-13 12:11

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('principal', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='comentario',
            options={'verbose_name': 'Comentario', 'verbose_name_plural': 'Comentarios'},
        ),
        migrations.AlterModelOptions(
            name='informacionpersonal',
            options={'verbose_name': 'Informacion Personal', 'verbose_name_plural': 'Informacion Personales'},
        ),
        migrations.AlterModelOptions(
            name='pregunta',
            options={'verbose_name': 'Pregunta', 'verbose_name_plural': 'Preguntas'},
        ),
        migrations.AlterModelOptions(
            name='respuesta',
            options={'verbose_name': 'Respuesta de Comentario', 'verbose_name_plural': 'Respuestas de Comentarios'},
        ),
        migrations.AddField(
            model_name='pregunta',
            name='usuid',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
