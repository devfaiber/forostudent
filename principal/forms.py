from django import forms
from django.contrib.auth.forms import UserCreationForm
from principal.models import InformacionPersonal
from django.contrib.auth.models import User

class RegistroUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = [
            "username",
            "email",
            "password",
        ]
        label = {
            "username": "Nombre de usuario",
            "email": "Correo Electronico",
            "password": "Contraseña",
        }

    def __init__(self, *args, **kwargs):
        super(RegistroUserForm, self).__init__(*args, **kwargs)

        for field in self.fields:

            self.fields[field].widget.attrs.update({'class': 'form-control'})

class EditUserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = [
            "username",
            "email"
        ]
        label = {
            "username": "Nombre de usuario",
            "email": "Correo Electronico"
        }

    def __init__(self, *args, **kwargs):
        super(EditUserForm, self).__init__(*args, **kwargs)

        for field in self.fields:

            self.fields[field].widget.attrs.update({'class': 'form-control'})

class RegistroDatosForm(forms.ModelForm):
    class Meta:
        model = InformacionPersonal

        fields = [
            "nombre",
            "apellidos",
            "tipo_documento",
            "num_documento",
            "rol",
            "foto",

        ]
        label = {
            "nombre": "Nombre",
            "apellidos": "Apellidos",
            "tipo_documento": "Tipo de documento",
            "num_documento": " Numero de documento",
            "rol": "Rol de usuario",
            "foto": "Fotografia"
        }

    def __init__(self,rol=None, *args, **kwargs):
        super(RegistroDatosForm, self).__init__(*args, **kwargs)

        if not rol is None:
            self.fields["rol"].initial = rol
            self.fields["rol"].widget = forms.HiddenInput()
            self.fields["rol"].widget.attrs.update({'class':'form-control'})

        for field in self.fields:

            self.fields[field].widget.attrs.update({'class': 'form-control'})
        self.fields["foto"].widget.attrs.update({'class': 'd-block'})