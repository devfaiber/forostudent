
from django.urls import path
from aprendiz import views

app_name = "aprendiz"
urlpatterns = [
    path('', views.Index.as_view(), name="index"),
    path('crear-preguntas/', views.CrearPregunta.as_view(), name="crearPreguntas"),
    path('editar_perfil/<int:pk>/', views.EditarPerfil.as_view(), name="editarPerfil"),
    path('eliminar_perfil/', views.EliminarPerfil.as_view(), name="eliminarPerfil"),
    path('preguntas/', views.VerPreguntas.as_view(), name="verPreguntas"),
    path('comentario/', views.InsertarComentario.as_view(), name="comentario")
]
