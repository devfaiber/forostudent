from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.shortcuts import render

# Create your views here.
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, UpdateView
from django.views.generic.base import View

from principal.forms import EditUserForm, RegistroDatosForm
from principal.models import InformacionPersonal, Pregunta, Comentario


class LoginAccess(LoginRequiredMixin):
    def dispatch(self, request, *args, **kwargs):

        if not request.user.is_authenticated:
            return HttpResponseRedirect(reverse("principal:index"))
        print(request.user.is_authenticated)
        datos = InformacionPersonal.objects.get(userid=request.user.pk)
        if datos.rol == "APR":
            return HttpResponseRedirect(reverse("aprendiz:index"))

        return super().dispatch(request, *args, **kwargs)


class Index(LoginAccess, View):
    login_url = '/'

    def get(self, request):
        datosPersonales = InformacionPersonal.objects.get(userid=request.user.pk)

        datos = {
            "nombre": datosPersonales.nombre,
            "apellido": datosPersonales.apellidos,
            "rol": datosPersonales.rol,
            "foto": datosPersonales.foto
        }

        return render(request, "instructor/index.html", datos)

class VerPreguntas(ListView):
    model = Pregunta
    template_name = "instructor/preguntas.html"

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)

        context["comentario"] = Comentario.objects.all()


        return context

class InsertarComentario(View):
    def get(self, request):
        return HttpResponseRedirect(reverse("instructor:index"))
    def post(self, request):
        datos = request.POST.get("comentario")
        id_pregunta = request.POST.get("id_preg")

        comentario = Comentario()
        comentario.userid = InformacionPersonal.objects.get(userid=request.user)
        comentario.contenido = datos
        comentario.pregid = Pregunta.objects.get(id=id_pregunta)

        comentario.save()
        return HttpResponseRedirect(reverse("instructor:verPreguntas"))

class VerAprendices(ListView):
    model = InformacionPersonal
    template_name = "instructor/aprendices.html"

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)

        context["object_list"] = InformacionPersonal.objects.filter(rol__iexact="APR")


        return context

class EditarPerfil(UpdateView):
    model = User
    template_name = "instructor/perfil_editar.html"
    form_class = RegistroDatosForm
    form2_class = EditUserForm
    success_url = reverse_lazy("instructor:index")

    def get_context_data(self, **kwargs):
        context = super(EditarPerfil, self).get_context_data(**kwargs)

        datos_personales = InformacionPersonal.objects.get(userid=self.request.user.pk)


        context['form'] = self.form_class(rol=datos_personales.rol,instance=datos_personales)
        if 'form2' not in context:
            context['form2'] = self.form2_class(instance=self.request.user)

        return context

    def post(self, request, *args, **kwargs):

        self.object = self.get_object
        datos_personales = InformacionPersonal.objects.get(userid=self.request.user.pk)
        rol = datos_personales.rol

        form = self.form_class(datos_personales.rol, self.request.POST, self.request.FILES, instance=datos_personales)


        form2 = self.form2_class(self.request.POST, instance=request.user)

        if form.is_valid() and form2.is_valid():
            form = form.save(commit=False)


            form.rol = rol
            form.save()
            form2.save()

            messages.add_message(request, messages.SUCCESS, "Has actualizado tu perfil con exito")
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.render_to_response(self.get_context_data(form=form, form2=form2))
