from django.contrib import admin
from principal.models import *
# Register your models here.
admin.site.register(InformacionPersonal)
admin.site.register(Comentario)
admin.site.register(Respuesta)
admin.site.register(Pregunta)