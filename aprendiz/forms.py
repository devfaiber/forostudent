from django import forms
from principal.models import Pregunta

class PreguntaForm(forms.ModelForm):
    class Meta:
        model = Pregunta
        fields = [
            "titulo",
            "descripcion"
        ]
        labels = {
            "titulo": "Titulo de la pregunta",
            "descripcion": "Descripcion detallada"
        }

    def __init__(self,*args, **kwargs):
        super(PreguntaForm, self).__init__(*args, **kwargs)

        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control'})