from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse_lazy, reverse
from django.views.generic import CreateView, UpdateView, ListView
from django.views.generic.base import View

from django.contrib.auth.models import User
from principal.models import Pregunta, InformacionPersonal, Comentario
from aprendiz.forms import PreguntaForm
from principal.forms import RegistroDatosForm, EditUserForm

# Create your views here.


class LoginAccess(LoginRequiredMixin):
    def dispatch(self, request, *args, **kwargs):

        if not request.user.is_authenticated:
            return HttpResponseRedirect(reverse("principal:index"))

        try:
            datos = InformacionPersonal.objects.get(userid=request.user.pk)
            if datos.rol == "INS":
                return HttpResponseRedirect(reverse("instructor:index"))

        except InformacionPersonal.DoesNotExist:
            print("No se cargaron los datos del perfil")

        return super().dispatch(request, *args, **kwargs)


class Index(LoginAccess, View):
    login_url = '/'
    def get(self, request):
        try:
            datosPersonales = InformacionPersonal.objects.get(userid=request.user.pk)

            datos = {
                "nombre": datosPersonales.nombre,
                "apellido": datosPersonales.apellidos,
                "rol": datosPersonales.rol,
                "foto": datosPersonales.foto
            }

            return render(request, "aprendiz/index.html", datos)
        except InformacionPersonal.DoesNotExist:
            return HttpResponseRedirect("principal:index")


class CrearPregunta(CreateView):
    model = Pregunta
    template_name = "aprendiz/realizar_pregunta.html"
    form_class = PreguntaForm
    success_url = reverse_lazy("aprendiz:index")

    def post(self, request, *args, **kwargs):
        self.object = self.get_object
        form = self.form_class(self.request.POST)

        if form.is_valid():
            datosPregunta = form.save(commit=False)

            info = InformacionPersonal.objects.get(userid=self.request.user.id)

            datosPregunta.userid = info
            datosPregunta.save()
            messages.add_message(request, messages.SUCCESS, "Tu pregunta ha sido posteada en el muro del instructor")
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.render_to_response(self.get_context_data(form=form))


class EditarPerfil(UpdateView):
    model = User
    template_name = "aprendiz/perfil_editar.html"
    form_class = RegistroDatosForm
    form2_class = EditUserForm
    success_url = reverse_lazy("aprendiz:index")

    def get_context_data(self, **kwargs):
        context = super(EditarPerfil, self).get_context_data(**kwargs)

        datos_personales = InformacionPersonal.objects.get(userid=self.request.user.pk)
        context["rol"] = datos_personales.rol

        context['form'] = self.form_class(rol=datos_personales,instance=datos_personales)
        if 'form2' not in context:
            context['form2'] = self.form2_class(instance=self.request.user)

        return context

    def post(self, request, *args, **kwargs):

        self.object = self.get_object
        datos_personales = InformacionPersonal.objects.get(userid=self.request.user.pk)
        rol = datos_personales.rol

        form = self.form_class(datos_personales.rol,self.request.POST, self.request.FILES,instance=datos_personales)
        form2 = self.form2_class(self.request.POST, instance=request.user)

        if form.is_valid() and form2.is_valid():
            form = form.save(commit=False)

            form.rol = rol
            form.save()
            form2.save()


            messages.add_message(request, messages.SUCCESS, "Has actualizado tu perfil con exito")
            return HttpResponseRedirect(self.get_success_url())
        else:
            messages.add_message(request, messages.ERROR, "No se han llenado todos los campos")
            return self.render_to_response(self.get_context_data(form=form, form2=form2))

class EliminarPerfil(View):

    def get(self, request):
        url = request.GET.get("next","/accounts")

        return HttpResponseRedirect(url)

    def post(self, request):
        try:
            usuario = User.objects.get(pk=request.user.pk)
        except:
            messages.add_message(request, messages.ERROR, "Lo sentimos hay un error en este perfil, prueba denuevo o comunicate con el administrador")
            return HttpResponseRedirect(reverse("principal:index"))

        url = request.GET.get("next", "/accounts")


        if usuario.delete():
            messages.add_message(request, messages.SUCCESS, "Has eliminado este usuario con exito")
        else:
            messages.add_message(request, messages.ERROR, "Hubo un problema al eliminar el usuario")


        return HttpResponseRedirect(url)

class VerPreguntas(ListView):
    model = Pregunta
    template_name = "aprendiz/preguntas.html"

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            userid = InformacionPersonal.objects.get(userid=self.request.user.pk)

            context['object_list'] = Pregunta.objects.filter(userid=userid.pk)


            datos = InformacionPersonal.objects.get(userid=self.request.user.pk)


            context["comentario"] = Comentario.objects.filter(pregid__userid=userid.pk)
        except InformacionPersonal.DoesNotExist:
            messages.add_message(self.request,messages.WARNING, "Precaucion un error al cargar iformacion de perfil")
        return context

class InsertarComentario(View):
    def get(self,request):
        return HttpResponseRedirect(reverse("aprendiz:index"))
    def post(self,request):
        datos = request.POST.get("comentario")
        id_pregunta = request.POST.get("id_preg")

        comentario = Comentario()
        comentario.userid = InformacionPersonal.objects.get(userid=request.user)
        comentario.contenido = datos
        comentario.pregid = Pregunta.objects.get(id=id_pregunta)

        comentario.save()
        return HttpResponseRedirect(reverse("aprendiz:verPreguntas"))
